
// var webpMachine = new webpHero.WebpMachine()
// webpMachine.polyfillDocument()
const isMobile = (/Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
const root = document.documentElement;
// let cursor = document.getElementById("cursor");
// let cursorBorder = document.getElementById("cursorBorder");
gsap.registerPlugin(ScrollToPlugin, ScrollTrigger);
gsap.registerPlugin(ScrollTrigger);
ScrollTrigger.saveStyles(".mobile, .desktop");

    var mobileAndTabletcheck = function() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
    };

(function($, window, document, undefined) {

    'use strict';

    $(function() {

       

        TII.init();
        TII.mobileMenu();
        TII.homeSlider();
        TII.homeVideo();
        TII.homeGallery();
        TII.homeNews();

        ScrollTrigger.matchMedia({
            // GSAP for Desktop only 
	        "(min-width: 800px)": function() {
                TII.parallaxBg();
            }, 

        });
        
        TII.pageLoader();
        TII.aboutGallery();
        TII.teamSlider();
        TII.achieveSlider();
       

        $('.scrollArrow-top').on("click",function(){
            $('html, body').animate({scrollTop:0}, 'fast');
            return false;
        });

        TII.mediaIsotopOnclick();


       
       

    });

    


})($, window, document);


var TII = {
    init: function() {
        window.onscroll = function() {
            stickyheader()
        };
        var header = document.querySelector("header");
        var sticky = header.offsetTop;

        function stickyheader() {
            if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
            } else {
                header.classList.remove("sticky");
            }
        };

        $('.scrollArrow-top').on("click",function(){
            $('html, body').animate({scrollTop:0}, 'fast');
            return false;
        });
    },
    mobileMenu: function() {
        $(".hamburger").click(function(){
            $('body').toggleClass("is-active");
        });
    },
    homeSlider: function() {
        // Swiper
        var menu = ['Indy Autonomous Challenge (IAC)', 'TII-EuroRacing Team and the Autonomous Challenge @CES2022', 'Achievements at a Glance']

        var mySwiper = new Swiper(".homeBannerSwiper-container", {
            effect: 'fade',
            fadeEffect: {
              crossFade: true
            },
            loop: false,
            speed: 800,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev"
            },
            pagination: {
                el: '.swiper-pagination',
                      clickable: true,
                  renderBullet: function (index, className) {
                    return '<span class="' + className + '">' + (menu[index]) + '</span>';
                  },
              },
        });
        
    },
    homeVideo: function() {
        const player = new Plyr('#player',{
            controls: [
                'play-large', // The large play button in the center
            ],
        });

    },
    homeGallery: function() {
        new Swiper('.home-gallery-swiper', {
            speed: 800,
            loop: true,
            spaceBetween: 20,
            centeredSlides: false,
            slidesPerView: 3,
            slidesPerColumn: 1, // and this
            navigation: {
                nextEl: ".galleryswiper-button-next",
                prevEl: ".galleryswiper-button-prev"
            },
            breakpoints: {
                1920: {
                    slidesPerView:3,
                    spaceBetween: 20
                },
                1028: {
                    slidesPerView: 3,
                    spaceBetween: 20
                },
                480: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20
                }
            }
        });
    },
    homeNews: function() {
        new Swiper('.home-news-swiper', {
            speed: 800,
            loop: true,
            spaceBetween: 20,
            centeredSlides: false,
            slidesPerView: 4,
            navigation: {
                nextEl: ".newsswiper-button-next",
                prevEl: ".newsswiper-button-prev"
            },
            breakpoints: {
                1920: {
                    slidesPerView:4,
                    spaceBetween: 20
                },
                1028: {
                    slidesPerView: 4,
                    spaceBetween: 20
                },
                480: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20
                }
            }
        });
    },
    aboutGallery: function() {
        new Swiper('.about-swiper-slide', {
            speed: 800,
            loop: false,
            spaceBetween: 20,
            centeredSlides: false,
            slidesPerView: 3,
            navigation: {
                nextEl: ".aboutSwiper-button-next",
                prevEl: ".aboutSwiper-button-prev"
            },
            breakpoints: {
                1920: {
                    slidesPerView:3,
                    spaceBetween: 20
                },
                1028: {
                    slidesPerView: 3,
                    spaceBetween: 20
                },
                480: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20
                }
            }
        });
    },
    teamSlider: function() {
        new Swiper('.team-swiper-init', {
            speed: 800,
            loop: false,
            spaceBetween: 20,
            slidesPerView: 3,
            navigation: {
                nextEl: ".team-button-next",
                prevEl: ".team-button-prev"
            },
            breakpoints: {
                1920: {
                    slidesPerView:3,
                    spaceBetween: 20
                },
                1028: {
                    slidesPerView: 3,
                    spaceBetween: 20
                },
                480: {
                    slidesPerView: 3,
                    spaceBetween: 10
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20
                }
            }
        });
    },
    achieveSlider: function() {
        new Swiper('.achieve-swiper-init', {
            speed: 800,
            loop: false,
            spaceBetween: 20,
            slidesPerView: 1.2,            
            navigation: {
                nextEl: ".achieve-button-next",
                prevEl: ".achieve-button-prev"
            },
            breakpoints: {
                1920: {
                    spaceBetween: 20
                },
                1028: {
                    spaceBetween: 20
                },
                480: {
                    spaceBetween: 10
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20
                }
            }
        });
    },
    parallaxBg: function() {
        gsap.config({
            autoSleep: 60,
            force3D: false,
            nullTargetWarn: false,
            trialWarn: false,
            units: {left: "%", top: "%", rotation: "rad"}
          });

        const tl = gsap.timeline({scrollTrigger:{
            trigger:".hm-banner-content",
            scrub: true,
            pin:false,
            start:"top top",
            end:"+=100%",
            markers:false        
        }});

        tl.to('.bannerImage', {
            y: -100,
            scale:1.1,
            duration:8,
            force3D:true,
            
        },'-=8')
        tl.to('.innerBannerImage', {
            y: -50,
            x: -60,
            duration:8,
            force3D:true,
            
        },'-=8')
        tl.to('.transtionOne', {
            y: -200,
            duration:8
        },'-=7')
        .to('.transtionTwo',  {
            y: -200,
            duration:8
        }, '-=7')
        .to('.transtionThree',  {
            y: -200,
            duration:8
        }, '-=7')
        .to('.swiper-pagination-bullet',  {
            y: -300,
            stagger: 0.3,
            duration:8
        }, '-=8')
        .from('.transtionFive',  {
            y: 300,
            autoAlpha: 0,
            duration:8
        }, '-=3')
       
        

        //  common fadeIn animation
         const fadeInAnim = gsap.utils.toArray('.fadeInTop');
         fadeInAnim.forEach((section) => {
                gsap.from(section, { 
                    autoAlpha: 0,
                    y: 150,
                    duration: 1,
                    stagger: 0.3,
                    ease: "power1.inOut",
                    scrollTrigger: {
                        trigger: section,
                        start: "0 90%",
                        end:"bottom 0",
                        markers: false,
                        once: true
                    }
            });
          });

        // About Banner Animate

        const tlAbout = gsap.timeline({scrollTrigger:{
            trigger:".inner-banner-content",
            scrub: true,
            pin:false,
            start:"top top",
            end:"+=100%",
            markers:false        
        }});

        tlAbout.to('.transtionSix', {
            y: -100,
            duration:6
        },'-=9')
        .to('.transtionSeven', {
            y: -70,
            duration:6
        },'-=7');


        // About Challenge Section Animate

        const tlChallenge = gsap.timeline({scrollTrigger:{
            trigger:".about-challenge",
            scrub: true,
            pin:false,
            start:"top 50%",
            end:"+=100%",
            markers:false        
        }});

        tlChallenge.to('.about-challenge-imgOne', {
            y: -50,
            duration:6
        },'-=7')
        .to('.about-challenge-imgTwo', {
            y: -50,
            duration:6
        },'-=4');

        // About Big image  Section Animate

        const tlBigImage = gsap.timeline({scrollTrigger:{
            trigger:".image-full-view",
            scrub: true,
            pin:false,
            start:"top 50%",
            end:"+=100%",
            once: true,
            markers:false        
        }});

        tlBigImage.to('.image-full-view h2', {
            y: -50,
            duration:6
        },'-=7')
        .from('.image-full-view h2',  {
            autoAlpha: 0,
            duration:8
        }, '-=7')
        .to('.image-full-view p', {
            y: -50,
            stagger: 0.3,
            duration:6
        },'-=4')
        .from('.image-full-view p',  {
            autoAlpha: 0,
            stagger: 0.3,
            duration:8
        }, '-=4');
        
    },
    pageLoader:function(){

        const tllLoader = gsap.timeline({
            paused: "true"
        });
        tllLoader.to("#percent, #bar",{
            duration:.2,
            opacity: 0,
            zIndex: -1
        });
        tllLoader.to("#preloader",{
            duration: .8,
            width: "0%"
        });
        
        var width = 1;
        var bar = document.getElementById("barconfrm");
        var id;
        function move(){
            id = setInterval(frame,10);
    
        }
        move();
        function frame(){
            if(width>=100){
                clearInterval(id);
                tllLoader.play();
            }
            else{
                width++;
                bar.style.width = width + "%";
                document.getElementById("percent").innerHTML = width + "%";
            }
        }
        
    },

    isotopeGallery:function(){
        // external js: isotope.pkgd.js, cells-by-column.js, cells-by-row.js, fit-columns.js, horizontal.js, masonry-horizontal.js

        // init Isotope
        var $grid = $('.grid').isotope({
            // itemSelector: '.grid-item',
          
            // // gutterWidth: 10,

                itemSelector: ".grid-item",
                columnWidth: ".grid-item",
                // percentPosition: true,
                gutter: 10,
                fitWidth: true,
                layoutMode: 'masonry',

        });
        
        var isHorizontal = false;
        var $window = $( window );
        
        // $('.layout-mode-button-group').on( 'click', 'button', function() {
        //     // adjust container sizing if layout mode is changing from vertical or horizontal
        //     var $this = $(this);
        //     var isHorizontalMode = !!$this.attr('data-is-horizontal');
        //     if ( isHorizontal !== isHorizontalMode ) {
        //     // change container size if horiz/vert change
        //     var containerStyle = isHorizontalMode ? {
        //         height: $window.height() * 0.7
        //     } : {
        //         width: 'auto'
        //     };
        //     $grid.css( containerStyle );
        //     isHorizontal = isHorizontalMode;
        //     }
        //     // change layout mode
        //     var layoutModeValue = $this.attr('data-layout-mode');
        //     $grid.isotope({ layoutMode: layoutModeValue });
        // });  
        
        // change is-checked class on buttons
        // $('.button-group').each( function( i, buttonGroup ) {
        //     var $buttonGroup = $( buttonGroup );
        //     $buttonGroup.on( 'click', 'button', function() {
        //     $buttonGroup.find('.is-checked').removeClass('is-checked');
        //     $( this ).addClass('is-checked');
        //     });
        // });

        
        $("#pills-photos-tab").click(function () {
            $grid.isotope('destroy');
            setTimeout(function() { 
                $grid = $('.grid').isotope({
                    itemSelector: '.grid-item',
                    layoutMode: 'masonry',
                    // gutterWidth: 10,
                    // masonry: {
                    //     columnWidth: 5
                    // }
        
                });
                
            }, 800);
        });
    },

        mediaIsotopOnclick:function(){
            var href = window.location.href;
            if (href.indexOf("/index.html") != -1) {
            } else if (href.indexOf("/media.html#pills-photos-tab") != -1) {
                $("body").addClass("media-active");
                $('#pills-tab #pills-photos-tab').tab('show');
                setTimeout(function() { 
                    TII.isotopeGallery();
                }, 2000);
            } 
            
        }

}

$(window).on('load', function(){ 
    TII.isotopeGallery();

    
});
